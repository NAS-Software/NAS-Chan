class CreateCommentVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :comment_votes do |t|
      t.references :user
      t.references :comment
      t.integer :status
    end
  end
end
