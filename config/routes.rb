Rails.application.routes.draw do
	get '/', to: redirect('/boards')

	match '/auth/:provider/callback', to: 'authentication#login', via: :all
	get '/logout', to: 'authentication#logout', as: 'logout'

	resource :user, controller: :authentication

	resources :boards do
		resources :posts, except: :index do
			resources :comments do #-> url.com/boards/:id/posts/:post_id/comments/:id
				member do
					post 'vote'
				end
			end

			member do
				post 'vote'
			end
		end
	end

	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
