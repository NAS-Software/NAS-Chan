class User < ApplicationRecord
	has_many :comments, dependent: :destroy
	has_many :boards, dependent: :destroy

	def self.from_omni(auth)
		User.find_by(uid: auth[:uid]) || create_from_omni(auth)
	end

	def session
		session = {
				user_id: id,
				email: email
		}

		return session
	end

	def self.from_session session
		if session[:user] == nil
			return nil
		end

		user_id = session[:user]['user_id']
		if user_id == nil
			return nil
		end

		return User.find_by! id: user_id
	end

	private
	def self.create_from_omni(auth)
		create! do |u|
			u.uid = auth[:uid]
			u.email = auth[:info][:email]
			u.alias = u.email.split('@')[0]
		end
	end
end
