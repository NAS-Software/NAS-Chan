class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.references :parent, foreign_key: {to_table: :comments}
      t.string :commenter
      t.text :body
      t.references :post, foreign_key: true
    end
  end
end
