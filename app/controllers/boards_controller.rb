class BoardsController < ApplicationController
	def index
		@boards = Board.all
	end

	def show
		@board = Board.find params[:id]
		@posts = @board.posts

	end

	def new
		authenticate_user
	end

	def create
		user = authenticate_user
		return if user == nil

		@board = Board.create!(params.require(:board).permit(:name)) do |b|
			b.owner = user
		end

		redirect_to @board
	end

	def destroy
		user = authenticate_user
		return if user == nil

		@board = Board.find params[:id]

		if @board.owner != user
			redirect_to board_path(@board)
			return
		end

		@board.delete
		redirect_to boards_path
	end

end
