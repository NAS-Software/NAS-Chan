class CreatePostVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :post_votes do |t|
      t.references :user
      t.references :post
      t.integer :status
    end
  end
end
