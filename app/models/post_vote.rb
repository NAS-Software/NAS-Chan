require 'voted_model'

class PostVote < ApplicationRecord
	belongs_to :user
	belongs_to :post

	extend VoteModel
	init_status
end
