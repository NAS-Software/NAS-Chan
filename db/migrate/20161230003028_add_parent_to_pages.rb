class AddParentToPages < ActiveRecord::Migration[5.0]
	def change
		change_table :pages do |t|
			t.references :pages, foreign_key: true
		end
	end
end
