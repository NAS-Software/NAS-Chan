require 'voted_model'

class CommentVote < ApplicationRecord
	belongs_to :user
	belongs_to :comment

	extend VoteModel
	init_status
end
