require 'voted_model'

class Post < ApplicationRecord
	belongs_to :board
	belongs_to :owner, class_name: 'User'
	has_many :comments, dependent: :destroy
	validates :title, presence: true, length: { minimum: 4 }
	has_many :post_votes, dependent: :destroy

	include VotedModel

	def votes
		post_votes
	end
end
