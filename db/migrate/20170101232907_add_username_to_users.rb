class AddUsernameToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      # setting the alias here because the auth isn't set up to create usernames (at least to my knowledge)
      t.string :alias, :default => "testName"
    end
  end
end
