module VotedModel
	def votes
		raise 'VotedModel.votes not implemented!'
	end

	def vote_score
		s = 0

		votes.each do |c|
			if c.up_vote?
				s += 1
			elsif c.down_vote?
				s -= 1
			end
		end

		s
	end

	def order_score
		vote_score
	end
end

module VoteModel
	def init_status
		enum status: [:up_vote, :down_vote]
	end
end
