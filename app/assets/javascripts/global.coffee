window.app = {
	data: {
		callbacks: {

		}
	}
	on_action: (ctrl, act, callbk) ->
		callbacks = this.data.callbacks
		callbacks[ctrl] = {} if callbacks[ctrl] == undefined
		callbacks[ctrl][act] = [] if callbacks[ctrl][act] == undefined
		callbacks[ctrl][act].push(callbk)
}

$(document).on "turbolinks:load", ->
# Run our callbacks
	callbacks = window.app.data.callbacks
	td = window.app.teledata
	if callbacks[td.controller] != undefined
		if callbacks[td.controller][td.action] != undefined
			callback() for callback in callbacks[td.controller][td.action]

	# dropdown menu
	$('.dropdown').click (o) ->
		obj = $(o.target)
		box = obj.children().first()
		box.slideToggle()

