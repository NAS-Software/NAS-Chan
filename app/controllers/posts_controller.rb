class PostsController < ApplicationController
	def show
		@post = Post.find(params[:id])
		@comments = @post.comments.where parent: nil
	end

	def new
		@board = Board.find params[:board_id]
		authenticate_user
	end

	def create
		user = authenticate_user
		return if user == nil

		@board = Board.find params[:board_id]

		@post = Post.create! params.require(:post).permit(:title, :contents) do |p|
			p.board = @board
			p.owner = user
		end

		redirect_to board_post_path(@board, @post)
	end

	def edit
	end

	def update
	end

	# TODO: Merge with vote in comments_controller
	def vote
		user = authenticate_user
		return if user == nil

		@post = Post.find(params[:id])
		mode = params[:mode]

		@post.post_votes.where(post: @post, user: user).destroy_all

		if mode == 'up_vote' or mode == 'down_vote'
			@post.post_votes.create(post: @post, user: user, status: mode.to_sym)
		end
	end

	def destroy
		user = authenticate_user
		return if user == nil

		@post = Post.find params[:id]

		if @post.owner != user
			redirect_to board_post_path(@post.board, @post)
			return
		end

		@post.delete
		redirect_to board_path(@post.board)
	end

end
