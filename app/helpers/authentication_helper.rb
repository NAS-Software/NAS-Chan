module AuthenticationHelper
	def current_user
		User.from_session session
	end
end
