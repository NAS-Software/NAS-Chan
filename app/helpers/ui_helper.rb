module UiHelper
	def dropdown_box(&block)
		panel = content_tag :div,
												capture(&block),
												class: 'dropdown-panel'

		content_tag :span,
								'&#x25BC;'.html_safe + panel,
								class: 'dropdown'
	end
end
