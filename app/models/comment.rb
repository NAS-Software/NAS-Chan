require 'voted_model'

class Comment < ApplicationRecord
	belongs_to :post
	belongs_to :user
	belongs_to :parent, class_name: 'Comment', optional: true
	has_many :replies, class_name: 'Comment', foreign_key: :parent_id, dependent: :destroy
	has_many :comment_votes, dependent: :destroy

	include VotedModel

	def display_name viewing_user
		if anonymous
			return 'Anonymous (me)' if viewing_user == user
			return 'Anonymous'
		end

		return user.alias
	end

	def votes
		comment_votes
	end
end
