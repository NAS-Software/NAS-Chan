class CommentsController < ApplicationController

	def create
		@post = Post.find(params[:post_id])

		user = User.from_session session
		if user == nil
			redirect_back fallback_location: board_post_path(@post.board, @post), alert: 'You must be logged in to comment'
			return
		end

		@comment = @post.comments.create! comment_params do |c|
			c.user = user
		end

		# redirects to the sender (in this case the post)
		redirect_back fallback_location: board_post_path(@post.board, @post)
	end

	def destroy
		user = authenticate_user
		return if user == nil

		@comment = Comment.find(params[:id])
		@post = @comment.post

		if @comment.user != user
			redirect_back fallback_location: board_post_path(@post.board, @post), alert: 'You do not own this post!'
			return
		end

		# delete the comment
		@comment.destroy

		# reload the previous page
		redirect_back fallback_location: board_post_path(@post.board, @post)
	end

	def edit
		@comment = Comment.find(params[:id])
		@post = @comment.post
	end

	def update
		@comment = Comment.find(params[:id])

		if @comment.update(comment_params)
			redirect_to board_post_path(@comment.post.board, @comment.post)
		else
			render 'edit'
		end
	end

	# TODO: Merge with vote in posts_controller
	def vote
		user = authenticate_user
		return if user == nil

		@comment = Comment.find(params[:id])
		mode = params[:mode]

		@comment.comment_votes.where(comment: @comment, user: user).destroy_all

		if mode == 'up_vote' or mode == 'down_vote'
			@comment.comment_votes.create(comment: @comment, user: user, status: mode.to_sym)
		end
	end

	private
	def comment_params
		params.require(:comment).permit(:parent_id, :commenter, :body, :anonymous)
	end
end
