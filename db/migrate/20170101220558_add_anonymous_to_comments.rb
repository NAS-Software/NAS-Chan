class AddAnonymousToComments < ActiveRecord::Migration[5.0]
	def change
		change_table :comments do |t|
			t.boolean :anonymous
		end
	end
end
