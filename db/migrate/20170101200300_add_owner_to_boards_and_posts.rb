class AddOwnerToBoardsAndPosts < ActiveRecord::Migration[5.0]
  def change
    change_table :boards do |t|
      t.references :owner, foreign_key: {to_table: :users}
    end
    change_table :posts do |t|
      t.references :owner, foreign_key: {to_table: :users}
    end
  end
end
