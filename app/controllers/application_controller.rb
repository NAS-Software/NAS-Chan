class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Simple auth helper for all controllers.
  def authenticate_user
    user = User.from_session session
    if user == nil
      redirect_to '/auth/nas_auth'
      return nil
    end
    # I like explicitly saying 'return' its more readable
    return user
  end
end
