class Board < ApplicationRecord
	has_many :posts, dependent: :destroy
	belongs_to :owner, class_name: 'User'
end
