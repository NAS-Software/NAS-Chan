class AddOwnerToComments < ActiveRecord::Migration[5.0]
	def change
		change_table :comments do |t|
			t.references :user, foreign_key: true
			t.remove :commenter
		end
	end
end
