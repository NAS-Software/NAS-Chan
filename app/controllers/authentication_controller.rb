class AuthenticationController < ApplicationController
	skip_before_filter :verify_authenticity_token

	def login
		@user = User.from_omni(auth_hash)

		session[:user] = @user.session

		redirect_to boards_path
	end

	def logout
		session[:user] = nil

		redirect_to boards_path
	end

	def edit
		@user = authenticate_user
	end

	def update
		@user = authenticate_user
		#check if the alias is already taken
		if User.where(alias: params[:user][:alias]).empty?
			@user.update(alias: params[:user][:alias])
		else
			flash.alert = "User name/alias taken"
		end

		redirect_to user_path
	end

	def show
		@user = authenticate_user
	end

	def destroy
		return unless authenticate_user

		logout

		@user.destroy
	end

	protected

	def auth_hash
		request.env['omniauth.auth']
	end
end
