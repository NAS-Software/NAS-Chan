$ -> window.app.on_action "posts", "show", ->
# do stuff here, for the post#show page load.
	$('.reply-bttn').click (e)->
		reply = $(e.target)

		id = reply.parent().data('comment-id')
		form = $('#edit_comment').parent().clone()
		form.removeAttr('id')
		form.find('#comment_parent_id').val(id)

		form.find('.cancel-reply-bttn').click (e)->
			form.before(reply)
			form.remove()

		reply.before(form)
		reply.detach()

	$('.vote-bttn').click (e)->
		bttn = $(e.target)
		post = bttn.parent()
		id = post.data('comment-id')
		url = post.data('comment-path')
		$.post url, {mode: bttn.data('vote')}
		location.reload()
	#	Broken, for some reason: Turbolinks.visit(window.location.href , { action: 'replace' })

	$('.post-vote-bttn').click (e)->
		bttn = $(e.target)
		url = window.location.href
		url += '/' unless url.endsWith('/')
		url += 'vote'
		$.post url, {mode: bttn.data('vote')}
		location.reload()
