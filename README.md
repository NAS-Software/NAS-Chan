<!-- this can be changed later, don't worry-->
[![License](https://img.shields.io/badge/License-AGPL-green.svg)](License.txt)

# NAS-Chan


The following project is the source code for the <small>[insert website name here]</small>
site.

### General Info
NAS-Chan (or just NAS) is a 4Chan/Reddit clone built with Ruby on Rails,
made by A couple of Randoms from the internet. The [Broad TODO](#Broad TODO)
outlines the general Idea of this site, but is still subject to change.

## Installation/Dependencies
<small>Note: This project comes with pre-configured 
Intellij project files, simply importing this 
project into a Jetbrains IDE like `RubyMine` will 
preconfigure required Gems and deps (hopefully). You would need to 
have the Ruby and Rails plugins installed.</small> 




Built Ruby Version: `2.3.3`.

Gems required: see the Gemfile


## Documentation

##### Starting the application
Assuming you have the required Gems, navigate to /bin and run
```
rails server
```
This will start the Ruby on Rails server by default on `http://localhost:3000`


TODO.. noob guide

## Broad TODO
<!-- put an - [x] into the box to check it-->
- [ ] Implement board/posts system
- [ ] Authentication system
- [ ] Karma system?
- [ ] Comments

 

<!--
* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
-->


